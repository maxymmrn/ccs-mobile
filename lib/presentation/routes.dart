import 'package:flutter/material.dart';
import 'package:ccs_mobile/domain/state/address.dart';
import 'package:ccs_mobile/presentation/pages/create_thermostat_page.dart';
import 'package:ccs_mobile/presentation/pages/auth/login_page.dart';
import 'package:ccs_mobile/presentation/pages/auth/register_page.dart';
import 'package:ccs_mobile/presentation/pages/dashboard_settings_page.dart';
import 'package:ccs_mobile/presentation/pages/details_page.dart';
import 'package:ccs_mobile/presentation/pages/home/home_page.dart';
import 'package:ccs_mobile/presentation/pages/pairing_page.dart';

mixin Routes {
  static Future<void> pushReplacementLogin(BuildContext context) =>
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (_) => const LoginPage()),
      );

  static Future<void> pushReplacementRegister(BuildContext context) =>
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (_) => const RegisterPage()),
      );

  static Future<void> pushReplacementHome(BuildContext context) =>
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (_) => const HomePage()),
      );

  static Future<void> pushDashboardSettings(BuildContext context) =>
      Navigator.push(
        context,
        MaterialPageRoute(builder: (_) => const DashboardSettingsPage()),
      );

  static Future<void> pushDetails(BuildContext context, StateAddress address) =>
      Navigator.push(
        context,
        MaterialPageRoute(builder: (_) => DetailsPage(address)),
      );

  static Future<void> pushPairing(BuildContext context) => Navigator.push(
        context,
        MaterialPageRoute(builder: (_) => const PairingPage()),
      );

  static Future<void> pushCreateThermostat(
    BuildContext context,
    StateAddress sensor,
  ) =>
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (_) => CreateThermostatPage(sensor: sensor),
        ),
      );
}
