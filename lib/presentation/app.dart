import 'package:flutter/material.dart';
import 'package:ccs_mobile/infrastructure/auth_service.dart';
import 'package:ccs_mobile/presentation/pages/auth/login_page.dart';
import 'package:ccs_mobile/presentation/pages/home/home_page.dart';
import 'package:ccs_mobile/presentation/style/font.dart';
import 'package:ccs_mobile/presentation/style/color.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => MaterialApp(
        title: 'Climate control',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          colorScheme: ColorScheme.fromSwatch(
            primarySwatch: AppColors.primary,
          ),
          primarySwatch: AppColors.primary,
          primaryColor: AppColors.swatch[400],
          primaryColorLight: AppColors.swatch[200],
          primaryColorDark: AppColors.swatch[600],
          errorColor: AppColors.error,
          backgroundColor: AppColors.background,
          scaffoldBackgroundColor: AppColors.background,
          bottomAppBarColor: AppColors.background,
          cardColor: AppColors.swatch[300],
          cardTheme: CardTheme(
            color: AppColors.background,
            elevation: 0,
            shape: RoundedRectangleBorder(
              side: BorderSide(color: AppColors.swatchNeutral[100]!, width: 1),
              borderRadius: const BorderRadius.all(Radius.circular(12)),
            ),
          ),
          chipTheme: ChipThemeData(
            backgroundColor: AppColors.swatchNeutral[100],
            selectedColor: AppColors.swatch[100],
            labelStyle: Styles.semiBold14(AppColors.swatchNeutral[800]),
            secondaryLabelStyle: Styles.semiBold14(AppColors.primary),
            disabledColor: AppColors.swatchNeutral[50],
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(8)),
            ),
          ),
          inputDecorationTheme: const InputDecorationTheme(
            border: OutlineInputBorder(
              borderSide: BorderSide(color: AppColors.primary),
              borderRadius: BorderRadius.all(Radius.circular(12)),
            ),
          ),
        ),
        darkTheme: ThemeData(
          colorScheme: ColorScheme.fromSwatch(
            primarySwatch: AppColors.primaryDark,
          ),
          primarySwatch: AppColors.primaryDark,
          primaryColor: AppColors.swatchDark[400],
          primaryColorLight: AppColors.swatchDark[200],
          primaryColorDark: AppColors.swatchDark[600],
          errorColor: AppColors.errorDark,
          backgroundColor: AppColors.backgroundDark,
          scaffoldBackgroundColor: AppColors.backgroundDark,
          bottomAppBarColor: AppColors.backgroundDark,
          cardColor: AppColors.swatchDark[300],
        ),
        themeMode: ThemeMode.light,
        home: AuthService.instance.isSignedIn
            ? const HomePage()
            : const LoginPage(),
      );
}
