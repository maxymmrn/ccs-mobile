import 'package:flutter/cupertino.dart';

abstract class Styles {
  static TextStyle bold10([Color? color]) {
    return TextStyle(
      fontSize: 10,
      fontFamily: 'NotoSans-Bold',
      height: 2,
      fontWeight: FontWeight.bold,
      color: color,
    );
  }

  static TextStyle bold12([Color? color]) {
    return TextStyle(
      fontSize: 12,
      fontFamily: 'NotoSans-Bold',
      height: 2,
      fontWeight: FontWeight.bold,
      color: color,
    );
  }

  static TextStyle regular14([Color? color]) {
    return TextStyle(
      fontSize: 14,
      fontFamily: 'NotoSans-Regular',
      letterSpacing: 0.15,
      height: 1.5,
      color: color,
    );
  }

  static TextStyle semiBold14([Color? color]) {
    return TextStyle(
      fontSize: 14,
      fontFamily: 'NotoSans-Regular',
      fontWeight: FontWeight.w600,
      color: color,
    );
  }

  static TextStyle regular16([Color? color]) {
    return TextStyle(
      fontSize: 16,
      fontFamily: 'NotoSans-Regular',
      letterSpacing: 0.1,
      height: 1.5,
      color: color,
    );
  }

  static TextStyle semiBold16([Color? color]) {
    return TextStyle(
      fontSize: 16,
      fontFamily: 'NotoSans-Regular',
      fontWeight: FontWeight.w600,
      color: color,
    );
  }

  static TextStyle semiBold18([Color? color]) {
    return TextStyle(
      fontSize: 18,
      fontFamily: 'NotoSans-Regular',
      fontWeight: FontWeight.w600,
      color: color,
    );
  }

  static TextStyle regular20([Color? color]) {
    return TextStyle(
      fontSize: 20,
      height: 1.2,
      fontFamily: 'NotoSans-Regular',
      color: color,
    );
  }

  static TextStyle semiBold20([Color? color]) {
    return TextStyle(
      fontSize: 20,
      fontFamily: 'NotoSans-SemiBold',
      color: color,
    );
  }

  static TextStyle semiBold24([Color? color]) {
    return TextStyle(
      fontSize: 24,
      letterSpacing: -0.15,
      height: 1.5,
      fontFamily: 'NotoSans-SemiBold',
      color: color,
    );
  }

  static TextStyle semiBold32([Color? color]) {
    return TextStyle(
      fontSize: 32,
      fontFamily: 'NotoSans-SemiBold',
      color: color,
    );
  }

  static TextStyle semiBold64([Color? color]) {
    return TextStyle(
      fontSize: 64,
      height: 1.2,
      fontFamily: 'NotoSans-SemiBold',
      color: color,
    );
  }
}
