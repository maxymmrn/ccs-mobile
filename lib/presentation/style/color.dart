import 'package:flutter/material.dart';

abstract class AppColors {
  static const swatch = {
    50: Color(0xFFEDEFFF),
    100: Color(0xFFDDE0FF),
    200: Color(0xFFBCC3FF),
    300: Color(0xFF9DA6F8),
    400: Color(0xFF828BDB),
    500: Color(0xFF6770BE),
    600: Color(0xFF5058A4),
    700: Color(0xFF373F8B),
    800: Color(0xFF1E2672),
    900: Color(0xFF00095D),
  };

  static const swatchPink = {
    50: Color(0xFFFBEDFF),
    100: Color(0xFFF7DDFF),
    200: Color(0xFFFBCFFF),
    300: Color(0xFFE39DF8),
    400: Color(0xFFC682DB),
    500: Color(0xFFAA67BE),
    600: Color(0xFF9050A4),
    700: Color(0xFF77378B),
    800: Color(0xFF5E1E72),
    900: Color(0xFF47005D),
  };

  static const swatchViolet = {
    50: Color(0xFF10002E),
    100: Color(0xFF21005D),
    200: Color(0xFF381E72),
    300: Color(0xFF4F378B),
    400: Color(0xFF6750A4),
    500: Color(0xFF7F67BE),
    600: Color(0xFF9A82DB),
    700: Color(0xFFB69DF8),
    800: Color(0xFFD0BCFF),
    900: Color(0xFFEADDFF),
  };

  static const swatchNeutral = {
    50: Color(0xFFF4EFEF),
    100: Color(0xFFE6E1E1),
    200: Color(0xFFCAC5C5),
    300: Color(0xFFAEAAAA),
    400: Color(0xFF949090),
    500: Color(0xFF797575),
    600: Color(0xFF625D5D),
    700: Color(0xFF494646),
    800: Color(0xFF333030),
    900: Color(0xFF1F1B1B),
  };

  static const swatchDark = {
    900: Color(0xFFEADDFF),
    800: Color(0xFFD0BCFF),
    700: Color(0xFFB69DF8),
    600: Color(0xFF9A82DB),
    500: Color(0xFF7F67BE),
    400: Color(0xFF6750A4),
    300: Color(0xFF4F378B),
    200: Color(0xFF381E72),
    100: Color(0xFF21005D),
    50: Color(0xFF10002E),
  };

  static const primary = MaterialColor(0xFF5058A4, swatch);
  static const primaryDark = MaterialColor(0xFF5058A4, swatchDark);

  static const error = Color(0xFFB3261E);
  static const errorDark = Color(0xFFF2B8B5);

  static const background = Color(0xFFFFFBFE);
  static const backgroundDark = Color(0xFF1C1B1F);
}
