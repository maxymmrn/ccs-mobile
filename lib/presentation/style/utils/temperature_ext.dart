extension TemperatureX on num {
  T map<T>({
    required T Function() freeze,
    required T Function() cold,
    required T Function() cool,
    required T Function() warm,
    required T Function() heat,
  }) {
    if (this < 0) {
      return freeze();
    } else if (this < 9) {
      return cold();
    } else if (this < 18) {
      return cool();
    } else if (this < 28) {
      return warm();
    } else {
      return heat();
    }
  }
}
