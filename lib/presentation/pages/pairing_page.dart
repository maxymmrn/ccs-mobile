import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:ccs_mobile/infrastructure/auth_service.dart';
import 'package:ccs_mobile/infrastructure/configuration_service.dart';
import 'package:ccs_mobile/presentation/style/color.dart';
import 'package:ccs_mobile/presentation/style/font.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:rxdart/rxdart.dart';

class PairingPage extends StatefulWidget {
  const PairingPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PairingPageState();
}

class _PairingPageState extends State<PairingPage> {
  bool _scanning = false;
  Barcode? result;
  QRViewController? controller;
  StreamSubscription? _subscription;

  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller?.pauseCamera();
    }
    controller?.resumeCamera();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(backgroundColor: Colors.transparent),
      body: Column(
        children: [
          Expanded(flex: 4, child: _buildQrView(context)),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                if (result != null)
                  Text(
                    'Код пристрою: ${result?.code}',
                    style: Styles.semiBold14(),
                  )
                else
                  Text(
                    'Проскануйте QR-код на пристрої',
                    style: Styles.semiBold14(),
                  ),
                TextButton(
                  child: FutureBuilder<bool?>(
                    future: controller?.getFlashStatus(),
                    builder: (context, snapshot) {
                      final turnedOn = snapshot.data ?? false;
                      return Text(
                        turnedOn ? "Вимкнути сплах" : "Увімкнути спалах",
                        style: Styles.semiBold14(AppColors.background),
                      );
                    },
                  ),
                  style: TextButton.styleFrom(
                    backgroundColor: AppColors.primary,
                    fixedSize: const Size(328, 40),
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                    ),
                  ),
                  onPressed: () async {
                    await controller?.toggleFlash();
                    setState(() {});
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildQrView(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final scanArea = (size.width < 400 || size.height < 400) ? 200 : 300;
    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      onPermissionSet: (ctrl, p) => _onPermissionSet(context, ctrl, p),
      overlay: QrScannerOverlayShape(
        borderColor: AppColors.background,
        borderRadius: 0,
        borderLength: 30,
        borderWidth: 12,
        cutOutSize: scanArea.toDouble(),
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    setState(() {
      this.controller = controller;
      if (Platform.isAndroid) {
        controller.pauseCamera();
      }
      controller.resumeCamera();
    });

    _subscription = controller.scannedDataStream
        .distinctUnique(hashCode: (barcode) => barcode.code.hashCode)
        .listen(_onScan);
  }

  void _onScan(Barcode scanData) {
    setState(() => result = scanData);
    final code = scanData.code;
    if (code != null && !_scanning) {
      setState(() => _scanning = true);
      _tryPair(code);
    }
  }

  Future<void> _tryPair(String code) async {
    try {
      final token = await AuthService.instance.getToken();
      if (token != null) {
        final data = code.split(':');
        if (data.length == 2) {
          final unitId = data.first;
          final pass = data.last;
          if (await ConfigurationService.instance.pair(token, unitId, pass)) {
            _subscription?.cancel();
            Navigator.pop(context);
          }
        }
      }
    } catch (e) {
      print(e);
    } finally {
      setState(() => _scanning = true);
    }
  }

  void _onPermissionSet(BuildContext context, QRViewController ctrl, bool p) {
    if (!p) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Не надано доступ')),
      );
    }
  }

  @override
  void dispose() {
    controller?.dispose();
    _subscription?.cancel();
    super.dispose();
  }
}
