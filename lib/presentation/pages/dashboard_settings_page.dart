import 'package:flutter/material.dart';

class DashboardSettingsPage extends StatelessWidget {
  const DashboardSettingsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Dashboard Settings'),
        ),
      );
}
