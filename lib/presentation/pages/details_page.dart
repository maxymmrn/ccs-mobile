import 'package:ccs_mobile/domain/state/type.dart';
import 'package:flutter/material.dart';
import 'package:ccs_mobile/presentation/routes.dart';
import 'package:ccs_mobile/domain/state/address.dart';
import 'package:ccs_mobile/infrastructure/state_service.dart';
import 'package:ccs_mobile/presentation/style/color.dart';
import 'package:ccs_mobile/presentation/style/font.dart';
import 'package:ccs_mobile/presentation/style/utils/temperature_ext.dart';
import 'package:ccs_mobile/presentation/widgets/button/solid_button.dart';
import 'package:ccs_mobile/presentation/widgets/tiles/thermostat_control_tile.dart';

class DetailsPage extends StatelessWidget {
  final StateAddress address;

  const DetailsPage(this.address, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        body: CustomScrollView(
          slivers: [
            SliverAppBar(
              stretch: true,
              pinned: true,
              collapsedHeight: 56,
              expandedHeight: 126,
              iconTheme: const IconThemeData(color: AppColors.background),
              flexibleSpace: const FlexibleSpaceBar(
                title: Text('Давач температури'),
              ),
              actions: [
                IconButton(
                  onPressed: () => _delete(context),
                  icon: const Icon(Icons.delete_outline),
                ),
              ],
            ),
            SliverToBoxAdapter(child: TemperatureCard(address)),
            SliverToBoxAdapter(
              child: ThermostatCard(
                sensor: address,
                thermostat: address.copyWith(type: StateType.comparator),
              ),
            ),
            const SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.all(16.0),
                child: TextField(
                  decoration: InputDecoration(label: Text('Кімната')),
                ),
              ),
            ),
            const SliverToBoxAdapter(child: ChartCard()),
          ],
        ),
      );

  Future<void> _delete(BuildContext context) => showDialog(
        context: context,
        builder: (context) => AlertDialog(
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12)),
          ),
          title: const Text('Підтвердіть видалення термостата'),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: Text(
                'Скасувати',
                style: Styles.semiBold14(AppColors.swatchNeutral[700]),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
              child: TextButton(
                onPressed: () => Navigator.pop(context),
                child: Text(
                  'Видалити',
                  style: Styles.semiBold14(AppColors.background),
                ),
                style: TextButton.styleFrom(
                  backgroundColor: AppColors.error,
                  minimumSize: const Size(108, 40),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
}

class TemperatureCard extends StatelessWidget {
  final StateAddress address;

  const TemperatureCard(this.address, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Card(
        margin: const EdgeInsets.fromLTRB(16, 24, 16, 8),
        child: SizedBox(
          height: 140,
          child: Center(
            child: StreamBuilder<num?>(
              stream: StateService.instance.watchStateValue(address),
              builder: (context, snapshot) {
                final temperature = snapshot.data;
                if (temperature != null) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            temperature.toStringAsFixed(1),
                            style: Styles.semiBold64(getColor(temperature)),
                          ),
                          Text(
                            '°C',
                            style: Styles.semiBold32(getColor(temperature)),
                          ),
                        ],
                      ),
                      const SizedBox(height: 4),
                      Text(
                        getType(temperature),
                        style: Styles.semiBold20(getColor(temperature)),
                      ),
                    ],
                  );
                } else {
                  return const CircularProgressIndicator();
                }
              },
            ),
          ),
        ),
      );

  String getType(num temperature) => temperature.map(
        freeze: () => 'Мороз',
        cold: () => 'Холодно',
        cool: () => 'Прохолодно',
        warm: () => 'Тепло',
        heat: () => 'Гаряче',
      );

  Color? getColor(num temperature) => temperature.map(
        freeze: () => AppColors.swatch[500],
        cold: () => AppColors.swatch[500],
        cool: () => AppColors.swatchViolet[500],
        warm: () => AppColors.swatchPink[500],
        heat: () => AppColors.swatchPink[500],
      );
}

class ThermostatCard extends StatelessWidget {
  final StateAddress sensor;
  final StateAddress thermostat;

  const ThermostatCard({
    Key? key,
    required this.thermostat,
    required this.sensor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => StreamBuilder<num?>(
        stream: StateService.instance.watchStateValue(thermostat),
        builder: (context, thermostatSnapshot) => StreamBuilder<num?>(
          stream: StateService.instance.watchStateValue(sensor),
          builder: (context, sensorSnapshot) {
            final temperature = sensorSnapshot.data?.toDouble();
            final value = thermostatSnapshot.data?.toDouble();
            if (temperature != null && value != null) {
              return ThermostatControlTile(
                address: thermostat,
                sensorValue: temperature,
                thermostatValue: value,
              );
            } else {
              return AddThermostatCard(sensor: sensor);
            }
          },
        ),
      );
}

class AddThermostatCard extends StatelessWidget {
  final StateAddress sensor;

  const AddThermostatCard({
    Key? key,
    required this.sensor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => SolidButton(
        label: 'Додати термостат',
        onPressed: () => Routes.pushCreateThermostat(context, sensor),
      );
}

class ChartCard extends StatelessWidget {
  const ChartCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Card(
        margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        child: SizedBox(
          height: 224,
          child: Center(
            child: Text(
              'Недостатньо даних\n'
              'для відораження графіку',
              style: Styles.semiBold14(AppColors.swatchNeutral[900]),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      );
}
