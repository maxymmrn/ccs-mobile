import 'package:ccs_mobile/presentation/style/color.dart';
import 'package:ccs_mobile/presentation/style/font.dart';
import 'package:flutter/material.dart';
import 'package:ccs_mobile/domain/state/address.dart';
import 'package:ccs_mobile/infrastructure/auth_service.dart';
import 'package:ccs_mobile/infrastructure/configuration_service.dart';


class CreateThermostatPage extends StatefulWidget {
  final StateAddress sensor;

  const CreateThermostatPage({
    Key? key,
    required this.sensor,
  }) : super(key: key);

  @override
  CreateThermostatPageState createState() => CreateThermostatPageState();
}

class CreateThermostatPageState extends State<CreateThermostatPage> {
  int? heaterPin;
  int? coolerPin;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(title: const Text('Створення термостата')),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _Dropdown(
                  title: 'Порт нагрівача',
                  value: heaterPin,
                  onChanged: (pin) => setState(() => heaterPin = pin),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16),
                  child: Divider(height: 1, color: AppColors.swatchNeutral[300]),
                ),
                _Dropdown(
                  title: 'Порт охолоджувача',
                  value: coolerPin,
                  onChanged: (pin) => setState(() => coolerPin = pin),
                ),
              ],
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: _buildFab(),
      );

  Widget? _buildFab() =>
      heaterPin != null && coolerPin != null && heaterPin != coolerPin
          ? FloatingActionButton.extended(
              onPressed: _save,
              label: const Text('Зберегти'),
              icon: const Icon(Icons.check),
            )
          : null;

  Future<void> _save() async {
    final token = await AuthService.instance.getToken();
    final _heaterPin = heaterPin;
    final _coolerPin = coolerPin;

    if (token != null && _heaterPin != null && _coolerPin != null) {
      final success = await ConfigurationService.instance.setThermostat(
        token,
        widget.sensor,
        _heaterPin,
        _coolerPin,
      );
      if (success) {
        Navigator.pop(context);
      }
    }
  }
}

class _Dropdown extends StatelessWidget {
  final String title;
  final int? value;
  final ValueChanged<int?> onChanged;

  const _Dropdown({
    Key? key,
    required this.title,
    required this.value,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Row(
        children: [
          Text(title, style: Styles.semiBold14(AppColors.swatchNeutral[900])),
          const SizedBox(width: 12),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            decoration: BoxDecoration(
              color: AppColors.swatch[100],
              borderRadius: const BorderRadius.all(Radius.circular(8)),
            ),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<int>(
                borderRadius: const BorderRadius.all(Radius.circular(8)),
                value: value,
                onChanged: onChanged,
                items: List.generate(10, (index) {
                  final p = index + 30;
                  return DropdownMenuItem(
                    value: p,
                    child: Text('$p', style: Styles.regular14()),
                  );
                }),
              ),
            ),
          ),
        ],
      );
}
