import 'package:ccs_mobile/infrastructure/auth_service.dart';
import 'package:ccs_mobile/presentation/routes.dart';
import 'package:ccs_mobile/presentation/style/color.dart';
import 'package:ccs_mobile/presentation/style/font.dart';
import 'package:ccs_mobile/presentation/widgets/button/outline_button.dart';
import 'package:ccs_mobile/presentation/widgets/button/solid_button.dart';
import 'package:ccs_mobile/presentation/widgets/tiles/room_tile.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserInfoView extends StatelessWidget {
  const UserInfoView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
        padding: const EdgeInsets.symmetric(vertical: 16),
        child: Column(
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 6),
              child: HomeCard(),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 6),
              child: ViewCard(),
            ),
            SolidButton(
              label: 'Просканувати новий пристрій',
              onPressed: () => Routes.pushPairing(context),
            ),
            AppOutlineButton(
              label: 'Вийти з аккаунта',
              onPressed: () => _logout(context),
            ),
          ],
        ),
      );

  Future<void> _logout(BuildContext context) => showDialog(
        context: context,
        builder: (context) => AlertDialog(
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12)),
          ),
          title: const Text('Підтвердіть вихід з акаунта'),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: Text(
                'Скасувати',
                style: Styles.semiBold14(AppColors.swatchNeutral[700]),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
              child: TextButton(
                onPressed: () async {
                  await AuthService.instance.signOut();
                  await (await SharedPreferences.getInstance()).clear();
                  await Routes.pushReplacementLogin(context);
                },
                child: Text(
                  'Вийти',
                  style: Styles.semiBold14(AppColors.background),
                ),
                style: TextButton.styleFrom(
                  backgroundColor: AppColors.primary,
                  minimumSize: const Size(108, 40),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
}

class HomeCard extends StatelessWidget {
  const HomeCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Card(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Мій будинок',
                    style: Styles.semiBold16(AppColors.swatchNeutral[900]),
                  ),
                  Text(
                    'Кімнати: 3',
                    style: Styles.semiBold14(AppColors.swatchNeutral[600]),
                  ),
                ],
              ),
              const SizedBox(height: 8),
              const RoomTile(name: 'Вітальня'),
              const RoomTile(name: 'Кухня'),
              const RoomTile(name: 'Ванна'),
              AppOutlineButton(
                label: 'Створити кімнату',
                onPressed: () => showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                    ),
                    title: const Text('Створення кімнати'),
                    content: const TextField(
                      autofocus: true,
                      decoration: InputDecoration(labelText: 'Назва кімнати'),
                    ),
                    actions: [
                      TextButton(
                        onPressed: () => Navigator.pop(context),
                        child: Text(
                          'Скасувати',
                          style: Styles.semiBold14(
                            AppColors.swatchNeutral[700],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 16,
                          vertical: 4,
                        ),
                        child: TextButton(
                          onPressed: () => Navigator.pop(context),
                          child: Text(
                            'Створити',
                            style: Styles.semiBold14(AppColors.background),
                          ),
                          style: TextButton.styleFrom(
                            backgroundColor: AppColors.primary,
                            minimumSize: const Size(108, 40),
                            shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(20),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
}

class ViewCard extends StatefulWidget {
  const ViewCard({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => ViewCardState();
}

class ViewCardState extends State<ViewCard> {
  @override
  Widget build(BuildContext context) => Card(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: FutureBuilder<SharedPreferences>(
            future: SharedPreferences.getInstance(),
            builder: (context, snapshot) {
              final prefs = snapshot.data;
              final view = prefs?.getString('view') ?? 'list';
              final temp = prefs?.getString('temperature') ?? 'C';
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Відображення даних',
                    style: Styles.semiBold16(AppColors.swatchNeutral[900]),
                  ),
                  const SizedBox(height: 16),
                  Divider(height: 1, color: AppColors.swatchNeutral[300]),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: Row(
                      children: [
                        Text(
                          'Вигляд',
                          style: Styles.semiBold14(
                            AppColors.swatchNeutral[900],
                          ),
                        ),
                        const SizedBox(width: 12),
                        ChoiceChip(
                          label: const Text('Список'),
                          selected: view == 'list',
                          onSelected: (_) async {
                            await prefs?.setString('view', 'list');
                            setState(() {});
                          },
                        ),
                        const SizedBox(width: 12),
                        ChoiceChip(
                          label: const Text('Картки'),
                          selected: view == 'grid',
                          onSelected: (_) async {
                            await prefs?.setString('view', 'grid');
                            setState(() {});
                          },
                        ),
                      ],
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Одиниці вимірювання',
                        style: Styles.semiBold14(AppColors.swatchNeutral[900]),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ChoiceChip(
                            label: const Text('°C'),
                            selected: temp == 'C',
                            onSelected: (_) async {
                              await prefs?.setString('temperature', 'C');
                              setState(() {});
                            },
                          ),
                          ChoiceChip(
                            label: const Text('K'),
                            selected: temp == 'K',
                            onSelected: (_) async {
                              await prefs?.setString('temperature', 'K');
                              setState(() {});
                            },
                          ),
                          ChoiceChip(
                            label: const Text('°F'),
                            selected: temp == 'F',
                            onSelected: (_) async {
                              await prefs?.setString('temperature', 'F');
                              setState(() {});
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              );
            },
          ),
        ),
      );
}
