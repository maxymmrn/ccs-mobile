import 'dart:async';

import 'package:flutter/material.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ccs_mobile/domain/state/address.dart';
import 'package:ccs_mobile/domain/state/type.dart';
import 'package:ccs_mobile/domain/state/value.dart';
import 'package:ccs_mobile/infrastructure/state_service.dart';
import 'package:ccs_mobile/presentation/routes.dart';
import 'package:ccs_mobile/presentation/style/color.dart';
import 'package:ccs_mobile/presentation/style/font.dart';
import 'package:ccs_mobile/presentation/widgets/button/solid_button.dart';
import 'package:ccs_mobile/presentation/widgets/tiles/temperature_sensor_tile.dart';

class TemperatureSensorsView extends StatefulWidget {
  const TemperatureSensorsView({Key? key}) : super(key: key);

  @override
  TemperatureSensorsViewState createState() => TemperatureSensorsViewState();
}

class TemperatureSensorsViewState extends State<TemperatureSensorsView> {
  @override
  Widget build(BuildContext context) => FutureBuilder<SharedPreferences>(
        future: SharedPreferences.getInstance(),
        builder: (context, snapshot) {
          final prefs = snapshot.data;
          if (prefs == null) {
            return const SizedBox();
          }
          final unitId = prefs.getString('unitId');
          if (unitId == null) {
            return _PairView(
              onClick: () async {
                await Routes.pushPairing(context);
                Timer(
                  const Duration(milliseconds: 300),
                  () => setState(() {}),
                );
              },
            );
          }
          return FirebaseAnimatedList(
            shrinkWrap: true,
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            query: StateService.instance.queryStates(
              unitId,
              StateType.temperature,
            ),
            defaultChild: const Align(
              alignment: Alignment.topCenter,
              child: SizedBox(child: LinearProgressIndicator(minHeight: 4)),
            ),
            itemBuilder: (_, snapshot, animation, index) {
              final _snapshotValue = snapshot.value as Map?;
              if (_snapshotValue != null) {
                return FadeTransition(
                  opacity: animation,
                  child: TemperatureSensorCard(
                    name: 'Давач температури $index',
                    value: StateValue.fromSnapshot(_snapshotValue),
                    address: StateAddress(
                      id: index,
                      type: StateType.temperature,
                      unitId: unitId,
                    ),
                  ),
                );
              }
              return const SizedBox();
            },
          );
        },
      );
}

class _PairView extends StatelessWidget {
  final VoidCallback onClick;

  const _PairView({
    Key? key,
    required this.onClick,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(height: 160),
            SvgPicture.asset(
              'assets/icons/scan.svg',
              width: 128,
              height: 128,
            ),
            const SizedBox(height: 20),
            Text(
              'Щоб побачити температуру на 1-wire давачах\n'
              '1. Увімкніть живлення на пристрої\n'
              '2. Проскануйте QR-код',
              style: Styles.semiBold14(AppColors.swatchNeutral[700]),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 12),
            SizedBox(
              width: 160,
              child: SolidButton(label: 'Просканувати', onPressed: onClick),
            ),
          ],
        ),
      );
}
