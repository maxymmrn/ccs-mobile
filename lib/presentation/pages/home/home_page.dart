import 'package:ccs_mobile/presentation/style/font.dart';
import 'package:flutter/material.dart';
import 'package:ccs_mobile/presentation/pages/home/temperature_sensors_view.dart';
import 'package:ccs_mobile/presentation/pages/home/user_info_view.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _controller = PageController();
  int _pageIndex = 0;

  @override
  Widget build(BuildContext context) =>
      NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (notification) {
          notification.disallowIndicator();
          return false;
        },
        child: Scaffold(
          appBar: AppBar(
            title: const Text('Клімат-контроль'),
            titleTextStyle: Styles.semiBold20(),
          ),
          body: PageView(
            controller: _controller,
            onPageChanged: _onPageSwipe,
            children: const [
              TemperatureSensorsView(),
              UserInfoView(),
            ],
          ),
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: _pageIndex,
            onTap: _onBottomBarClick,
            items: const [
              BottomNavigationBarItem(
                icon: Icon(Icons.thermostat_rounded),
                label: 'Дані',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person),
                label: 'Профіль',
              ),
            ],
          ),
        ),
      );

  void _onPageSwipe(int index) => setState(() {
        _pageIndex = index;
      });

  void _onBottomBarClick(int index) => setState(() {
        _pageIndex = index;
        _controller.jumpToPage(index);
      });
}
