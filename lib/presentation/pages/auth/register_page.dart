import 'package:flutter/material.dart';
import 'package:flutter_signin_button/button_list.dart';
import 'package:flutter_signin_button/button_view.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ccs_mobile/infrastructure/auth_service.dart';
import 'package:ccs_mobile/infrastructure/configuration_service.dart';
import 'package:ccs_mobile/presentation/routes.dart';
import 'package:ccs_mobile/presentation/style/color.dart';
import 'package:ccs_mobile/presentation/style/font.dart';
import 'package:ccs_mobile/presentation/widgets/button/outline_button.dart';
import 'package:ccs_mobile/presentation/widgets/button/solid_button.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  String _email = '';
  String _password = '';

  @override
  Widget build(BuildContext context) => Scaffold(
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
          padding: const EdgeInsets.only(top: 96),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(
                'assets/icons/round_logo.svg',
                width: 128,
                height: 128,
              ),
              const SizedBox(height: 16),
              Text(
                'Реєстрація',
                style: Styles.semiBold24(AppColors.primary),
              ),
              const SizedBox(height: 16),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16,
                  vertical: 8,
                ),
                child: TextFormField(
                  onChanged: (text) => _email = text,
                  decoration: const InputDecoration(
                    labelText: 'Електронна пошта',
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16,
                  vertical: 8,
                ),
                child: TextFormField(
                  obscureText: true,
                  onChanged: (text) => _password = text,
                  decoration: const InputDecoration(labelText: 'Пароль'),
                ),
              ),
              const SizedBox(height: 4),
              SolidButton(
                label: 'Зареєструватися',
                onPressed: () async {
                  final errorCode = await AuthService.instance.register(
                    _email,
                    _password,
                  );
                  if (errorCode == null) {
                    final token = await AuthService.instance.getToken();
                    if (token != null) {
                      final srv = ConfigurationService.instance;
                      final registered = await srv.registerUser(token);
                      if (registered) {
                        await Routes.pushReplacementHome(context);
                      }
                    }
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text(errorCode),
                    ));
                  }
                },
              ),
              const SizedBox(height: 4),
              AppOutlineButton(
                label: 'Увійти в акаунт',
                onPressed: () => Routes.pushReplacementLogin(context),
              ),
              const SizedBox(height: 8),
              SignInButton(
                Buttons.Google,
                text: 'Використати Google',
                elevation: 0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(200),
                  side: BorderSide(color: AppColors.swatchNeutral[200]!),
                ),
                padding: const EdgeInsets.symmetric(
                  horizontal: 56,
                  vertical: 6,
                ),
                onPressed: () {},
              ),
            ],
          ),
        ),
      );
}
