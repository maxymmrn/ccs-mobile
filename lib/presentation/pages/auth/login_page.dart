import 'package:ccs_mobile/presentation/style/color.dart';
import 'package:ccs_mobile/presentation/style/font.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:ccs_mobile/infrastructure/auth_service.dart';
import 'package:ccs_mobile/presentation/routes.dart';
import 'package:ccs_mobile/presentation/widgets/button/outline_button.dart';
import 'package:ccs_mobile/presentation/widgets/button/solid_button.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String _email = '';
  String _password = '';

  @override
  Widget build(BuildContext context) => Scaffold(
        resizeToAvoidBottomInset: true,
        body: SafeArea(
          child: SingleChildScrollView(
            padding: const EdgeInsets.only(top: 96),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  'assets/icons/round_logo.svg',
                  width: 128,
                  height: 128,
                ),
                const SizedBox(height: 16),
                Text('Вхід', style: Styles.semiBold24(AppColors.primary)),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 16,
                    vertical: 8,
                  ),
                  child: TextFormField(
                    onChanged: (text) => _email = text,
                    decoration: const InputDecoration(
                      labelText: 'Електронна пошта',
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 16,
                    vertical: 8,
                  ),
                  child: TextFormField(
                    obscureText: true,
                    onChanged: (text) => _password = text,
                    decoration: const InputDecoration(labelText: 'Пароль'),
                  ),
                ),
                const SizedBox(height: 4),
                SolidButton(
                  label: 'Увійти',
                  onPressed: () async {
                    final errorCode = await AuthService.instance.signIn(
                      _email,
                      _password,
                    );
                    if (errorCode == null) {
                      await Routes.pushReplacementHome(context);
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(errorCode),
                      ));
                    }
                  },
                ),
                const SizedBox(height: 4),
                AppOutlineButton(
                  label: 'Зареєструватися',
                  onPressed: () => Routes.pushReplacementRegister(context),
                ),
                const SizedBox(height: 8),
                SignInButton(
                  Buttons.Google,
                  text: 'Використати Google',
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(200),
                    side: BorderSide(color: AppColors.swatchNeutral[200]!),
                  ),
                  elevation: 0,
                  padding: const EdgeInsets.symmetric(
                    horizontal: 56,
                    vertical: 6,
                  ),
                  onPressed: () {},
                ),
              ],
            ),
          ),
        ),
      );
}
