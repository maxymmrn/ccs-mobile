import 'package:flutter/material.dart';
import 'package:ccs_mobile/domain/state/address.dart';
import 'package:ccs_mobile/domain/state/value.dart';
import 'package:ccs_mobile/presentation/routes.dart';
import 'package:ccs_mobile/presentation/style/color.dart';
import 'package:ccs_mobile/presentation/style/font.dart';

class TemperatureSensorDashboardCard extends StatelessWidget {
  final String name;
  final StateAddress address;
  final StateValue value;

  const TemperatureSensorDashboardCard({
    Key? key,
    required this.name,
    required this.address,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 8),
        child: InkWell(
          onTap: () => Routes.pushDetails(context, address),
          child: GridTile(
            header: Text(name, style: Styles.semiBold14()),
            footer: Text(name, style: Styles.bold12()),
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    '${value.value.toStringAsFixed(1)}°C',
                    style: Styles.semiBold24(
                        getValueColor(value.value.toDouble())),
                  ),
                  Text(name,
                      style: Styles.bold12(AppColors.swatchNeutral[900])),
                  Text(name,
                      style: Styles.bold10(AppColors.swatchNeutral[800])),
                ],
              ),
              decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  borderRadius: const BorderRadius.all(Radius.circular(12)),
                  border: Border.all(color: AppColors.swatchNeutral[100]!)),
            ),
          ),
        ),
      );
}

getValueColor(double value) {
  if (value <= 8.0) {
    return AppColors.swatch[500];
  }
  if (value <= 22.0) {
    return AppColors.swatchViolet[500];
  } else {
    return AppColors.swatchPink[500];
  }
}
