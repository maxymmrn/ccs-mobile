import 'package:flutter/material.dart';
import 'package:ccs_mobile/domain/state/address.dart';
import 'package:ccs_mobile/domain/state/value.dart';
import 'package:ccs_mobile/presentation/routes.dart';
import 'package:ccs_mobile/presentation/style/color.dart';
import 'package:ccs_mobile/presentation/style/font.dart';

class TemperatureSensorCard extends StatelessWidget {
  final String name;
  final StateAddress address;
  final StateValue value;

  const TemperatureSensorCard({
    Key? key,
    required this.name,
    required this.address,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 8),
        child: ListTile(
          onTap: () => Routes.pushDetails(context, address),
          title: Text(name, style: Styles.semiBold14()),
          subtitle: Text('Кімнату не вказано', style: Styles.bold12()),
          trailing: Text(
            '${value.value.toStringAsFixed(1)}°C',
            style: Styles.semiBold24(getValueColor(value.value.toDouble())),
          ),
          shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12, width: 1.0),
            borderRadius: BorderRadius.all(Radius.circular(12)),
          ),
        ),
      );
}

getValueColor(double value){
  if (value <= 8.0) {
    return AppColors.swatch[500];
  }
  if (value <= 22.0) {
    return AppColors.swatchViolet[500];
  }
  else{
    return AppColors.swatchPink[500];
  }
}

