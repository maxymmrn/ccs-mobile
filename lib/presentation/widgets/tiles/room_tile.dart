import 'package:flutter/material.dart';
import 'package:ccs_mobile/presentation/style/color.dart';
import 'package:ccs_mobile/presentation/style/font.dart';

class RoomTile extends StatelessWidget {
  final String name;

  const RoomTile({Key? key, required this.name}) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        padding: const EdgeInsets.symmetric(vertical: 8),
        decoration: BoxDecoration(
          border: Border(top: BorderSide(color: AppColors.swatchNeutral[300]!)),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(name, style: Styles.semiBold14(AppColors.swatchNeutral[600])),
            InkWell(
              onTap: () => _showDialog(context),
              borderRadius: const BorderRadius.all(Radius.circular(16)),
              child: const Padding(
                padding: EdgeInsets.all(4),
                child: Icon(
                  Icons.delete_outline,
                  color: AppColors.error,
                  size: 24,
                ),
              ),
            ),
          ],
        ),
      );

  Future<void> _showDialog(BuildContext context) => showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: const Text('Підтвердіть видалення кімнати'),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12)),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: Text(
                'Скасувати',
                style: Styles.semiBold14(AppColors.swatchNeutral[700]),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
              child: TextButton(
                onPressed: () => Navigator.pop(context),
                child: Text(
                  'Видалити',
                  style: Styles.semiBold14(AppColors.background),
                ),
                style: TextButton.styleFrom(
                  backgroundColor: AppColors.error,
                  minimumSize: const Size(108, 40),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
}
