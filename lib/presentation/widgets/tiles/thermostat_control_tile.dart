import 'package:ccs_mobile/domain/state/address.dart';
import 'package:ccs_mobile/infrastructure/auth_service.dart';
import 'package:ccs_mobile/infrastructure/configuration_service.dart';
import 'package:flutter/material.dart';
import 'package:ccs_mobile/presentation/style/font.dart';
import 'package:ccs_mobile/presentation/style/color.dart';

class ThermostatControlTile extends StatefulWidget {
  final StateAddress address;
  final double sensorValue;
  final double thermostatValue;

  const ThermostatControlTile({
    Key? key,
    required this.address,
    required this.sensorValue,
    required this.thermostatValue,
  }) : super(key: key);

  @override
  ThermostatControlTileState createState() => ThermostatControlTileState();
}

class ThermostatControlTileState extends State<ThermostatControlTile> {
  num? _value;

  num get value => _value ?? widget.thermostatValue;

  @override
  Widget build(BuildContext context) {
    var swatch = AppColors.swatch;
    var state = 'Охолодження';
    if (widget.sensorValue <= value) {
      swatch = AppColors.swatchPink;
      state = 'Нагрівання';
    }
    var fillColor = swatch[500];
    var backgroundColor = swatch[100];
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: backgroundColor,
                ),
                child: IconButton(
                  onPressed: () => _setValue(value + 0.5),
                  icon: Icon(Icons.add, color: fillColor),
                  color: backgroundColor,
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        value.toStringAsFixed(1),
                        style: Styles.semiBold64(fillColor),
                      ),
                      Text('°C', style: Styles.semiBold32(fillColor)),
                    ],
                  ),
                  const SizedBox(height: 4),
                  Text(state, style: Styles.semiBold20(fillColor)),
                ],
              ),
              Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: backgroundColor,
                ),
                child: IconButton(
                  onPressed: () => _setValue(value - 0.5),
                  icon: Icon(Icons.remove, color: fillColor),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _setValue(double value) async {
    final token = await AuthService.instance.getToken();
    if (token != null) {
      final success = await ConfigurationService.instance.setValue(
        token,
        widget.address,
        value,
      );
      if (success) {
        setState(() => _value = value);
      }
    }
  }
}
