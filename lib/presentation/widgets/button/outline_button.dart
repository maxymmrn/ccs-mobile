import 'package:flutter/material.dart';
import 'package:ccs_mobile/presentation/style/color.dart';
import 'package:ccs_mobile/presentation/style/font.dart';

class AppOutlineButton extends StatelessWidget {
  final String label;
  final VoidCallback onPressed;

  const AppOutlineButton({
    Key? key,
    required this.label,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
        child: TextButton(
          child: Text(label, style: Styles.semiBold14(AppColors.primary)),
          onPressed: onPressed,
          style: TextButton.styleFrom(
            backgroundColor: AppColors.background,
            minimumSize: const Size(328, 48),
            side: const BorderSide(color: AppColors.primary, width: 0.5),
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(24)),
            ),
          ),
        ),
      );
}
