import 'package:ccs_mobile/presentation/style/color.dart';
import 'package:ccs_mobile/presentation/style/font.dart';
import 'package:flutter/material.dart';

class SolidButton extends StatelessWidget {
  final String label;
  final VoidCallback onPressed;

  const SolidButton({
    Key? key,
    required this.label,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
        child: TextButton(
          onPressed: onPressed,
          child: Text(label, style: Styles.semiBold14(AppColors.background)),
          style: TextButton.styleFrom(
            backgroundColor: AppColors.primary,
            minimumSize: const Size(328, 48),
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(24)),
            ),
          ),
        ),
      );
}
