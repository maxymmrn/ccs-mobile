// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'value.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

StateValue _$StateValueFromJson(Map<String, dynamic> json) {
  return _StateValue.fromJson(json);
}

/// @nodoc
mixin _$StateValue {
  num get value => throw _privateConstructorUsedError;
  bool get online => throw _privateConstructorUsedError;
  int get lastUpdate => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $StateValueCopyWith<StateValue> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StateValueCopyWith<$Res> {
  factory $StateValueCopyWith(
          StateValue value, $Res Function(StateValue) then) =
      _$StateValueCopyWithImpl<$Res>;
  $Res call({num value, bool online, int lastUpdate});
}

/// @nodoc
class _$StateValueCopyWithImpl<$Res> implements $StateValueCopyWith<$Res> {
  _$StateValueCopyWithImpl(this._value, this._then);

  final StateValue _value;
  // ignore: unused_field
  final $Res Function(StateValue) _then;

  @override
  $Res call({
    Object? value = freezed,
    Object? online = freezed,
    Object? lastUpdate = freezed,
  }) {
    return _then(_value.copyWith(
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as num,
      online: online == freezed
          ? _value.online
          : online // ignore: cast_nullable_to_non_nullable
              as bool,
      lastUpdate: lastUpdate == freezed
          ? _value.lastUpdate
          : lastUpdate // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$$_StateValueCopyWith<$Res>
    implements $StateValueCopyWith<$Res> {
  factory _$$_StateValueCopyWith(
          _$_StateValue value, $Res Function(_$_StateValue) then) =
      __$$_StateValueCopyWithImpl<$Res>;
  @override
  $Res call({num value, bool online, int lastUpdate});
}

/// @nodoc
class __$$_StateValueCopyWithImpl<$Res> extends _$StateValueCopyWithImpl<$Res>
    implements _$$_StateValueCopyWith<$Res> {
  __$$_StateValueCopyWithImpl(
      _$_StateValue _value, $Res Function(_$_StateValue) _then)
      : super(_value, (v) => _then(v as _$_StateValue));

  @override
  _$_StateValue get _value => super._value as _$_StateValue;

  @override
  $Res call({
    Object? value = freezed,
    Object? online = freezed,
    Object? lastUpdate = freezed,
  }) {
    return _then(_$_StateValue(
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as num,
      online: online == freezed
          ? _value.online
          : online // ignore: cast_nullable_to_non_nullable
              as bool,
      lastUpdate: lastUpdate == freezed
          ? _value.lastUpdate
          : lastUpdate // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_StateValue extends _StateValue {
  const _$_StateValue(
      {required this.value, required this.online, required this.lastUpdate})
      : super._();

  factory _$_StateValue.fromJson(Map<String, dynamic> json) =>
      _$$_StateValueFromJson(json);

  @override
  final num value;
  @override
  final bool online;
  @override
  final int lastUpdate;

  @override
  String toString() {
    return 'StateValue(value: $value, online: $online, lastUpdate: $lastUpdate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_StateValue &&
            const DeepCollectionEquality().equals(other.value, value) &&
            const DeepCollectionEquality().equals(other.online, online) &&
            const DeepCollectionEquality()
                .equals(other.lastUpdate, lastUpdate));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(value),
      const DeepCollectionEquality().hash(online),
      const DeepCollectionEquality().hash(lastUpdate));

  @JsonKey(ignore: true)
  @override
  _$$_StateValueCopyWith<_$_StateValue> get copyWith =>
      __$$_StateValueCopyWithImpl<_$_StateValue>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_StateValueToJson(this);
  }
}

abstract class _StateValue extends StateValue {
  const factory _StateValue(
      {required final num value,
      required final bool online,
      required final int lastUpdate}) = _$_StateValue;
  const _StateValue._() : super._();

  factory _StateValue.fromJson(Map<String, dynamic> json) =
      _$_StateValue.fromJson;

  @override
  num get value => throw _privateConstructorUsedError;
  @override
  bool get online => throw _privateConstructorUsedError;
  @override
  int get lastUpdate => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$$_StateValueCopyWith<_$_StateValue> get copyWith =>
      throw _privateConstructorUsedError;
}
