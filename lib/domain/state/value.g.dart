// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'value.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_StateValue _$$_StateValueFromJson(Map<String, dynamic> json) =>
    _$_StateValue(
      value: json['value'] as num,
      online: json['online'] as bool,
      lastUpdate: json['lastUpdate'] as int,
    );

Map<String, dynamic> _$$_StateValueToJson(_$_StateValue instance) =>
    <String, dynamic>{
      'value': instance.value,
      'online': instance.online,
      'lastUpdate': instance.lastUpdate,
    };
