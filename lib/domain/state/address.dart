import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:ccs_mobile/domain/state/type.dart';

part 'address.freezed.dart';

@freezed
class StateAddress with _$StateAddress {
  const StateAddress._();

  const factory StateAddress({
    required int id,
    required StateType type,
    required String unitId,
  }) = _StateAddress;

  String get path => '$unitId/${type.pathKey}/$id';
}
