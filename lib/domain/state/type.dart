enum StateType {
  unknown,
  oneWire,
  temperature,
  comparator,
}

extension StateTypeX on StateType {
  String get pathKey {
    switch (this) {
      case StateType.unknown:
        return 'UNKNOWN';
      case StateType.oneWire:
        return 'ONE_WIRE';
      case StateType.temperature:
        return 'TEMPERATURE';
      case StateType.comparator:
        return 'COMPARATOR';
    }
  }
}
