// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'address.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$StateAddress {
  int get id => throw _privateConstructorUsedError;
  StateType get type => throw _privateConstructorUsedError;
  String get unitId => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $StateAddressCopyWith<StateAddress> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StateAddressCopyWith<$Res> {
  factory $StateAddressCopyWith(
          StateAddress value, $Res Function(StateAddress) then) =
      _$StateAddressCopyWithImpl<$Res>;
  $Res call({int id, StateType type, String unitId});
}

/// @nodoc
class _$StateAddressCopyWithImpl<$Res> implements $StateAddressCopyWith<$Res> {
  _$StateAddressCopyWithImpl(this._value, this._then);

  final StateAddress _value;
  // ignore: unused_field
  final $Res Function(StateAddress) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? type = freezed,
    Object? unitId = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as StateType,
      unitId: unitId == freezed
          ? _value.unitId
          : unitId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_StateAddressCopyWith<$Res>
    implements $StateAddressCopyWith<$Res> {
  factory _$$_StateAddressCopyWith(
          _$_StateAddress value, $Res Function(_$_StateAddress) then) =
      __$$_StateAddressCopyWithImpl<$Res>;
  @override
  $Res call({int id, StateType type, String unitId});
}

/// @nodoc
class __$$_StateAddressCopyWithImpl<$Res>
    extends _$StateAddressCopyWithImpl<$Res>
    implements _$$_StateAddressCopyWith<$Res> {
  __$$_StateAddressCopyWithImpl(
      _$_StateAddress _value, $Res Function(_$_StateAddress) _then)
      : super(_value, (v) => _then(v as _$_StateAddress));

  @override
  _$_StateAddress get _value => super._value as _$_StateAddress;

  @override
  $Res call({
    Object? id = freezed,
    Object? type = freezed,
    Object? unitId = freezed,
  }) {
    return _then(_$_StateAddress(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as StateType,
      unitId: unitId == freezed
          ? _value.unitId
          : unitId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_StateAddress extends _StateAddress {
  const _$_StateAddress(
      {required this.id, required this.type, required this.unitId})
      : super._();

  @override
  final int id;
  @override
  final StateType type;
  @override
  final String unitId;

  @override
  String toString() {
    return 'StateAddress(id: $id, type: $type, unitId: $unitId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_StateAddress &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.type, type) &&
            const DeepCollectionEquality().equals(other.unitId, unitId));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(type),
      const DeepCollectionEquality().hash(unitId));

  @JsonKey(ignore: true)
  @override
  _$$_StateAddressCopyWith<_$_StateAddress> get copyWith =>
      __$$_StateAddressCopyWithImpl<_$_StateAddress>(this, _$identity);
}

abstract class _StateAddress extends StateAddress {
  const factory _StateAddress(
      {required final int id,
      required final StateType type,
      required final String unitId}) = _$_StateAddress;
  const _StateAddress._() : super._();

  @override
  int get id => throw _privateConstructorUsedError;
  @override
  StateType get type => throw _privateConstructorUsedError;
  @override
  String get unitId => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$$_StateAddressCopyWith<_$_StateAddress> get copyWith =>
      throw _privateConstructorUsedError;
}
