import 'package:freezed_annotation/freezed_annotation.dart';

part 'value.freezed.dart';

part 'value.g.dart';

@freezed
class StateValue with _$StateValue {
  const StateValue._();

  const factory StateValue({
    required num value,
    required bool online,
    required int lastUpdate,
  }) = _StateValue;

  factory StateValue.fromJson(Map<String, dynamic> json) =>
      _$StateValueFromJson(json);

  factory StateValue.fromSnapshot(Map json) => StateValue.fromJson({
        'value': json['value'],
        'online': json['online'],
        'lastUpdate': json['last_update'],
      });
}
