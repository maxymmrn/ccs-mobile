import 'package:ccs_mobile/domain/state/address.dart';
import 'package:ccs_mobile/domain/state/type.dart';
import 'package:ccs_mobile/domain/state/value.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:logging/logging.dart';

class StateService {
  final _logger = Logger('StateService');

  static StateService? _instance;

  static StateService get instance => _instance ??= StateService._();

  StateService._() {
    _logger.info('Instance $hashCode');
  }

  DatabaseReference queryStates(String unitId, StateType type) =>
      FirebaseDatabase.instance.ref('$unitId/${type.pathKey}');

  Stream<StateValue?> watchState(StateAddress address) {
    final ref = FirebaseDatabase.instance.ref(address.path);
    _logger.info(ref.path);
    return ref.onValue
        .map((event) => event.snapshot.value)
        .cast<Map<String, dynamic>?>()
        .map(_onState);
  }

  StateValue? _onState(Map<String, dynamic>? json) {
    _logger.info(json);
    if (json == null) {
      return null;
    }
    final _value = StateValue.fromSnapshot(json);
    return _value;
  }

  Stream<num?> watchStateValue(StateAddress address) {
    final ref = FirebaseDatabase.instance.ref(address.path).child('value');
    _logger.info(ref.path);
    return ref.onValue.map((event) => event.snapshot.value).cast<num?>();
  }
}
