import 'dart:convert';

import 'package:ccs_mobile/domain/state/address.dart';
import 'package:http/http.dart';
import 'package:logging/logging.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ConfigurationService {
  static const _baseUrl = 'core.ccs-back.bar';
  final _logger = Logger('PairingService');
  final Client _client;

  static ConfigurationService? _instance;

  static ConfigurationService get instance =>
      _instance ??= ConfigurationService._();

  ConfigurationService._() : _client = Client() {
    _logger.info('Instance $hashCode');
  }

  Future<bool> registerUser(String token) async {
    try {
      final url = Uri.https(_baseUrl, '/api/users/register');
      _logger.info('$url');

      final request = Request('POST', url);
      request.headers['Authorization'] = 'Bearer $token';
      request.headers['content-type'] = 'application/json';
      request.headers['accept'] = 'application/json';

      final response = await _client.send(request);
      final result = jsonDecode(await response.stream.bytesToString());
      _logger.info(result);
      return response.statusCode >= 200 && response.statusCode < 300;
    } catch (e) {
      _logger.warning('registerUser(): $e');
      return false;
    }
  }

  Future<bool> pair(String token, String unitId, String password) async {
    try {
      _logger.info(token);
      final url = Uri.https(_baseUrl, '/api/bridges/pair');
      final body = jsonEncode({'bridge_id': unitId, 'key': password});
      _logger.info('$url: $body');

      final request = Request('POST', url);
      request.headers['Authorization'] = 'Bearer $token';
      request.headers['content-type'] = 'application/json';
      request.headers['accept'] = 'application/json';
      request.body = body;

      final response = await _client.send(request);
      final result = jsonDecode(await response.stream.bytesToString());
      _logger.info(result);
      if (response.statusCode >= 200 && response.statusCode < 300) {
        final prefs = await SharedPreferences.getInstance();
        await prefs.setString('unitId', unitId);
        return true;
      }
      return false;
    } catch (e) {
      _logger.warning('pair($unitId, $password): $e');
      return false;
    }
  }

  Future<bool> setThermostat(
    String token,
    StateAddress sensor,
    int heaterPin,
    int coolerPin,
  ) async {
    try {
      _logger.info(token);
      final url = Uri.https(
        _baseUrl,
        '/api/bridges/${sensor.unitId}/thermostats',
      );
      final body = jsonEncode({
        'name': 'thermostat',
        'input_id': sensor.id,
        'heater': heaterPin,
        'cooler': coolerPin,
      });
      _logger.info('$url: $body');

      final request = Request('POST', url);
      request.headers['Authorization'] = 'Bearer $token';
      request.headers['content-type'] = 'application/json';
      request.headers['accept'] = 'application/json';
      request.body = body;

      final response = await _client.send(request);
      _logger.info(response.statusCode);
      return response.statusCode >= 200 && response.statusCode < 300;
    } catch (e) {
      _logger.warning('setThermostat($sensor, $heaterPin, $coolerPin): $e');
      return false;
    }
  }

  Future<bool> setValue(
    String token,
    StateAddress address,
    double value,
  ) async {
    try {
      _logger.info(token);
      final url = Uri.https(
        _baseUrl,
        '/api/bridges/${address.unitId}/thermostats/${address.id}/value',
      );
      final body = jsonEncode(value);
      _logger.info('$url: $body');

      final request = Request('POST', url);
      request.headers['Authorization'] = 'Bearer $token';
      request.headers['content-type'] = 'application/json';
      request.headers['accept'] = 'application/json';
      request.body = body;

      final response = await _client.send(request);
      _logger.info(response.statusCode);
      final result = jsonDecode(await response.stream.bytesToString());
      _logger.info(result);
      return response.statusCode >= 200 && response.statusCode < 300;
    } catch (e) {
      _logger.warning('setValue($address, $value): $e');
      return false;
    }
  }
}
