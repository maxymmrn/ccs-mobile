import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:logging/logging.dart';

class AuthService {
  final Logger _logger = Logger('AuthService');
  final GoogleSignIn _googleSignIn;
  UserCredential? _credential;

  static AuthService? _instance;

  AuthService._(this._googleSignIn) {
    _logger.info('Instance $hashCode');
  }

  static AuthService get instance =>
      _instance ??= AuthService._(GoogleSignIn());

  bool get isSignedIn => FirebaseAuth.instance.currentUser != null;

  Future<String?> register(String email, String password) async {
    try {
      _logger.info('Registering $email');
      _credential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      return null;
    } on FirebaseAuthException catch (e, trace) {
      _logger.warning(trace);
      return e.code;
    }
  }

  Future<String?> signIn(String email, String password) async {
    try {
      _logger.info('Signing In $email');
      _credential = await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      return null;
    } on FirebaseAuthException catch (e, trace) {
      _logger.warning(trace);
      return e.code;
    }
  }

  // TODO: check compatibility with Firebase tokens
  Future<String?> signInWithGoogle() async {
    _logger.info('Signing In with Google');
    await _googleSignIn.signIn();
    return null;
  }

  Future<void> signOut() {
    _logger.info('Signing Out');
    return Future.wait([
      FirebaseAuth.instance.signOut(),
      _googleSignIn.signOut(),
    ]);
  }

  Future<String?> getToken() async {
    final user = FirebaseAuth.instance.currentUser;
    final token = await user?.getIdToken();
    return token;
  }
}
